// To parse this JSON data, do
//
//     final GetProductsListPojo = GetProductsListPojoFromJson(jsonString);

import 'dart:convert';

GetProductsListPojo GetProductsListPojoFromJson(String str) =>
    GetProductsListPojo.fromJson(json.decode(str));

String GetProductsListPojoToJson(GetProductsListPojo data) =>
    json.encode(data.toJson());

class GetProductsListPojo {
  String status;
  String messages;
  List<Datum> data;

  GetProductsListPojo({
    this.status,
    this.messages,
    this.data,
  });

  factory GetProductsListPojo.fromJson(Map<String, dynamic> json) =>
      GetProductsListPojo(
        status: json["status"],
        messages: json["messages"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "messages": messages,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  int id;
  int categoryId;
  int userId;
  String subType;
  int noOfBedroom;
  int noOfBathroom;
  String furnishing;
  String listedBy;
  dynamic plotArea;
  dynamic plotLength;
  dynamic plotBreadth;
  String maintenance;
  int floorNo;
  String projectName;
  dynamic images;
  int noOfCarParking;
  int builtupArea;
  int carpetArea;
  int bachelorsAllowed;
  int totalFloors;
  String facing;
  String adTitle;
  String description;
  String price;
  int stateId;
  int cityId;
  int areaId;
  dynamic latitude;
  dynamic longitude;
  DateTime createdAt;
  DateTime updatedAt;
  User user;
  Category subCategory;
  Area area;

  Datum({
    this.id,
    this.categoryId,
    this.userId,
    this.subType,
    this.noOfBedroom,
    this.noOfBathroom,
    this.furnishing,
    this.listedBy,
    this.plotArea,
    this.plotLength,
    this.plotBreadth,
    this.maintenance,
    this.floorNo,
    this.projectName,
    this.images,
    this.noOfCarParking,
    this.builtupArea,
    this.carpetArea,
    this.bachelorsAllowed,
    this.totalFloors,
    this.facing,
    this.adTitle,
    this.description,
    this.price,
    this.stateId,
    this.cityId,
    this.areaId,
    this.latitude,
    this.longitude,
    this.createdAt,
    this.updatedAt,
    this.user,
    this.subCategory,
    this.area,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        categoryId: json["category_id"],
        userId: json["user_id"],
        subType: json["sub_type"],
        noOfBedroom: json["no_of_bedroom"],
        noOfBathroom: json["no_of_bathroom"],
        furnishing: json["furnishing"],
        listedBy: json["listed_by"],
        plotArea: json["plot_area"],
        plotLength: json["plot_length"],
        plotBreadth: json["plot_breadth"],
        maintenance: json["maintenance"],
        floorNo: json["floor_no"],
        projectName: json["project_name"],
        images: json["images"],
        noOfCarParking: json["no_of_car_parking"],
        builtupArea: json["builtup_area"],
        carpetArea: json["carpet_area"],
        bachelorsAllowed: json["bachelors_allowed"],
        totalFloors: json["total_floors"],
        facing: json["facing"],
        adTitle: json["ad_title"],
        description: json["description"],
        price: json["price"],
        stateId: json["state_id"],
        cityId: json["city_id"],
        areaId: json["area_id"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        user: json["user"] == null ? null : User.fromJson(json["user"]),
        subCategory: json["sub_category"] == null
            ? null
            : Category.fromJson(json["sub_category"]),
        area: json["area"] == null ? null : Area.fromJson(json["area"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "category_id": categoryId,
        "user_id": userId,
        "sub_type": subType,
        "no_of_bedroom": noOfBedroom,
        "no_of_bathroom": noOfBathroom,
        "furnishing": furnishing,
        "listed_by": listedBy,
        "plot_area": plotArea,
        "plot_length": plotLength,
        "plot_breadth": plotBreadth,
        "maintenance": maintenance,
        "floor_no": floorNo,
        "project_name": projectName,
        "images": images,
        "no_of_car_parking": noOfCarParking,
        "builtup_area": builtupArea,
        "carpet_area": carpetArea,
        "bachelors_allowed": bachelorsAllowed,
        "total_floors": totalFloors,
        "facing": facing,
        "ad_title": adTitle,
        "description": description,
        "price": price,
        "state_id": stateId,
        "city_id": cityId,
        "area_id": areaId,
        "latitude": latitude,
        "longitude": longitude,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "user": user.toJson(),
        "sub_category": subCategory.toJson(),
        "area": area.toJson(),
      };
}

class Area {
  int id;
  int stateId;
  int cityId;
  String areaName;
  DateTime createdAt;
  DateTime updatedAt;
  StateMy state;
  City city;

  Area({
    this.id,
    this.stateId,
    this.cityId,
    this.areaName,
    this.createdAt,
    this.updatedAt,
    this.state,
    this.city,
  });

  factory Area.fromJson(Map<String, dynamic> json) => Area(
        id: json["id"],
        stateId: json["state_id"],
        cityId: json["city_id"],
        areaName: json["area_name"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        state: json["state"] == null ? null : StateMy.fromJson(json["state"]),
        city: json["city"] == null ? null : City.fromJson(json["city"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "state_id": stateId,
        "city_id": cityId,
        "area_name": areaName,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "state": state.toJson(),
        "city": city.toJson(),
      };
}

class City {
  int id;
  int stateId;
  String cityName;
  DateTime createdAt;
  DateTime updatedAt;

  City({
    this.id,
    this.stateId,
    this.cityName,
    this.createdAt,
    this.updatedAt,
  });

  factory City.fromJson(Map<String, dynamic> json) => City(
        id: json["id"],
        stateId: json["state_id"],
        cityName: json["city_name"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "state_id": stateId,
        "city_name": cityName,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}

class StateMy {
  int id;
  String name;
  DateTime createdAt;
  DateTime updatedAt;

  StateMy({
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
  });

  factory StateMy.fromJson(Map<String, dynamic> json) => StateMy(
        id: json["id"],
        name: json["name"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}

class Category {
  int id;
  String categoryName;
  int parentId;
  DateTime createdAt;
  DateTime updatedAt;
  Category category;

  Category({
    this.id,
    this.categoryName,
    this.parentId,
    this.createdAt,
    this.updatedAt,
    this.category,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        categoryName: json["category_name"],
        parentId: json["parent_id"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        category: json["category"] == null
            ? null
            : Category.fromJson(json["category"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "category_name": categoryName,
        "parent_id": parentId,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "category": category == null ? null : category.toJson(),
      };
}

class User {
  int id;
  String firstName;
  String lastName;
  String email;
  String countryCode;
  String mobileNo;
  int otp;
  String profileImage;
  String fileExtension;
  String url;
  DateTime createdAt;
  DateTime updatedAt;

  User({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.countryCode,
    this.mobileNo,
    this.otp,
    this.profileImage,
    this.fileExtension,
    this.url,
    this.createdAt,
    this.updatedAt,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        email: json["email"],
        countryCode: json["country_code"],
        mobileNo: json["mobile_no"],
        otp: json["otp"],
        profileImage: json["profile_image"],
        fileExtension: json["file_extension"],
        url: json["url"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "first_name": firstName,
        "last_name": lastName,
        "email": email,
        "country_code": countryCode,
        "mobile_no": mobileNo,
        "otp": otp,
        "profile_image": profileImage,
        "file_extension": fileExtension,
        "url": url,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
