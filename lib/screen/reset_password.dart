import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/models/reset_password_pojo.dart';
import 'package:rental_app/presenter/reset_password_presenter.dart';
import 'package:rental_app/screen/login.dart';
import 'package:rental_app/utils/my_constants.dart';
import 'package:rental_app/utils/my_utils.dart';

class ResetPassword extends StatefulWidget {
  ResetPassword({Key key, this.getMobileNo}) : super(key: key);

  final String getMobileNo;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _ResetPasswordStateful();
  }
}

class _ResetPasswordStateful extends State<ResetPassword>
    implements ResetPasswordContract {
  ResetPasswordresenter _resetPasswordresenter;
  String selectedCountryCode = "+91";
  //TextFiled controller for handle input event
  final textPhoneController = TextEditingController();
  final textPasswordController = TextEditingController();
  final textConfirmPasswordController = TextEditingController();

  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _confirm_passwordFocus = FocusNode();

  @override
  void initState() {
    //initialize presenter for api call
    _resetPasswordresenter = new ResetPasswordresenter(this);
    textPhoneController.text = widget.getMobileNo;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(
            AppLocalizations.of(context).translate('label_reset_password')),
      ),
      body: new Center(
        child: SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.only(
                left: MyConstants.layout_margin,
                right: MyConstants.layout_margin),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //Country code picker
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    widget.getMobileNo.isEmpty
                        ? new CountryCodePicker(
                            onChanged: _onCountryChange,
                            // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                            initialSelection: 'IN',
                            favorite: [selectedCountryCode, 'IN'],
                            // optional. Shows only country name and flag
                            showCountryOnly: false,
                            // optional. Shows only country name and flag when popup is closed.
                            // optional. aligns the flag and the Text left
                            alignLeft: false,
                          )
                        : Container(),
                    new Flexible(
                      child: new TextFormField(
                        controller: textPhoneController,
                        textInputAction: TextInputAction.done,
                        inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly
                        ],
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            hintText: AppLocalizations.of(context)
                                .translate("hint_phone_number"),
                            contentPadding: const EdgeInsets.all(10.0)),
                        onFieldSubmitted: (term) {},
                      ),
                    ),
                  ],
                ),

                TextFormField(
                  controller: textPasswordController,
                  obscureText: true,
                  textInputAction: TextInputAction.next,
                  focusNode: _passwordFocus,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(
                        context, _passwordFocus, _confirm_passwordFocus);
                  },
                  decoration: InputDecoration(
                    hintText:
                        AppLocalizations.of(context).translate('hint_password'),
                  ),
                ),
                TextFormField(
                  controller: textConfirmPasswordController,
                  obscureText: true,
                  textInputAction: TextInputAction.done,
                  focusNode: _confirm_passwordFocus,
                  onFieldSubmitted: (term) {
                    _submitClickValidation();
                  },
                  decoration: InputDecoration(
                    hintText: AppLocalizations.of(context)
                        .translate('hint_confirm_password'),
                  ),
                ),

                //Todo for manage space bewtween control
                new Container(
                  margin: new EdgeInsets.fromLTRB(
                      0.0,
                      MyConstants.vertical_control_space,
                      0.0,
                      MyConstants.vertical_control_space),
                ),

                RaisedButton(
                  onPressed: () {
                    _submitClickValidation();
                  },
                  child: Text(
                      AppLocalizations.of(context).translate("label_submit")),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///Validate submit click and then Api call
  void _submitClickValidation() {
    setState(() {
      if (!MyUtils().validatePassword(context, textPasswordController.text)) {
        return;
      }

      if (!MyUtils()
          .validatePassword(context, textConfirmPasswordController.text)) {
        return;
      }
      //Password & Confirm password same
      if (textPasswordController.text != textConfirmPasswordController.text) {
        MyUtils().toastdisplay(AppLocalizations.of(context)
            .translate('msg_same_password_confirm'));
        return;
      }

      // Check for internet
      MyUtils().check().then((intenet) {
        if (intenet != null && intenet) {
          _resetPasswordresenter.doResetPassword(
              context,
              textPhoneController.text,
              selectedCountryCode,
              textPasswordController.text,
              textConfirmPasswordController.text);
        } else {
          MyUtils().toastdisplay(
              AppLocalizations.of(context).translate('msg_no_internet'));
        }
      });
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    textPhoneController.dispose();
    textPasswordController.dispose();
    textConfirmPasswordController.dispose();
    super.dispose();
  }

  ///Handle TextForField focus change
  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  //Handle country code change listener
  void _onCountryChange(CountryCode countryCode) {
    //Todo : manipulate the selected country code here
    selectedCountryCode = countryCode.toString();
    print("New Country selected: " + countryCode.toString());
  }

  //Handle Api call error
  @override
  void onResetError(String errorTxt) {
    MyUtils().toastdisplay(errorTxt);
  }

  //Handle Api call success flow
  @override
  void onResetSuccess(ResetPasswordPojo pojoData) {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => MyLogin(title: ""),
        ));
  }
}
