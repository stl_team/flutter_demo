import 'dart:io';

import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/models/signup_pojo.dart';
import 'package:rental_app/models/user_pojo.dart';
import 'package:rental_app/presenter/signup_presenter.dart';
import 'package:rental_app/utils/my_constants.dart';
import 'package:rental_app/utils/my_utils.dart';

import 'otp_verification.dart';

class MySignUp extends StatefulWidget {
  MySignUp({Key key, this.title}) : super(key: key);

  final String title;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _MySignUpStateful();
  }
}

class _MySignUpStateful extends State<MySignUp> implements SignupContract {
  SignupPresenter _signupPresenter;

  File _image = null;

  String selectedCountryCode = "+91";
  //TextFiled controller for handle input event
  final textFirstnameController = TextEditingController();
  final textLastnameController = TextEditingController();
  final textEmailController = TextEditingController();
  final textPhoneController = TextEditingController();
  final textPasswordController = TextEditingController();
  final textConfirmPasswordController = TextEditingController();

  //For handle keybaord next and done event handle
  final FocusNode firstnameFocus = FocusNode();
  final FocusNode lastnameFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _confirm_passwordFocus = FocusNode();

  @override
  void initState() {
    //Init presenter for api call and handle
    _signupPresenter = new SignupPresenter(this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: new Center(
        child: SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.only(
                left: MyConstants.layout_margin,
                right: MyConstants.layout_margin),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    _optionsDialogBox();
                  },
                  child: _image == null
                      ? FlatButton(
                          child: Image(
                            image: AssetImage("graphics/pic_photo.png"),
                            width: MyConstants.profile_image_height_width,
                            height: MyConstants.profile_image_height_width,
                          ),
                          onPressed: () {
                            _optionsDialogBox();
                          },
                        )
                      : Image.file(
                          _image,
                          height: MyConstants.profile_image_height_width,
                          width: MyConstants.profile_image_height_width,
                        ),
                ),

                new Container(
                  margin: new EdgeInsets.fromLTRB(
                      0.0,
                      MyConstants.vertical_control_space,
                      0.0,
                      MyConstants.vertical_control_space),
                ),

                TextFormField(
                  controller: textFirstnameController,
                  textInputAction: TextInputAction.next,
                  focusNode: firstnameFocus,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(context, firstnameFocus, lastnameFocus);
                  },
                  decoration: InputDecoration(
                    hintText: AppLocalizations.of(context)
                        .translate('hint_first_name'),
                  ),
                ),
                TextFormField(
                  controller: textLastnameController,
                  textInputAction: TextInputAction.next,
                  focusNode: lastnameFocus,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(context, lastnameFocus, _emailFocus);
                  },
                  decoration: InputDecoration(
                    hintText: AppLocalizations.of(context)
                        .translate('hint_last_name'),
                  ),
                ),
                TextFormField(
                  controller: textEmailController,
                  textInputAction: TextInputAction.next,
                  focusNode: _emailFocus,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(context, _emailFocus, _phoneFocus);
                  },
                  decoration: InputDecoration(
                    hintText:
                        AppLocalizations.of(context).translate('hint_email'),
                  ),
                ),
                Row(
                  //Country code dropdown
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    new CountryCodePicker(
                      onChanged: _onCountryChange,
                      // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                      initialSelection: 'IN',
                      favorite: [selectedCountryCode, 'IN'],
                      // optional. Shows only country name and flag
                      showCountryOnly: false,
                      // optional. Shows only country name and flag when popup is closed.
                      // optional. aligns the flag and the Text left
                      alignLeft: false,
                    ),
                    new Flexible(
                      child: new TextFormField(
                        controller: textPhoneController,
                        textInputAction: TextInputAction.next,
                        inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly
                        ],
                        keyboardType: TextInputType.number,
                        focusNode: _phoneFocus,
                        onFieldSubmitted: (term) {
                          _fieldFocusChange(
                              context, _phoneFocus, _passwordFocus);
                        },
                        decoration: InputDecoration(
                          hintText: AppLocalizations.of(context)
                              .translate('hint_phone_number'),
                        ),
                      ),
                    ),
                  ],
                ),
                TextFormField(
                  controller: textPasswordController,
                  obscureText: true,
                  textInputAction: TextInputAction.next,
                  focusNode: _passwordFocus,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(
                        context, _passwordFocus, _confirm_passwordFocus);
                  },
                  decoration: InputDecoration(
                    hintText:
                        AppLocalizations.of(context).translate('hint_password'),
                  ),
                ),
                TextFormField(
                  controller: textConfirmPasswordController,
                  obscureText: true,
                  textInputAction: TextInputAction.done,
                  focusNode: _confirm_passwordFocus,
                  onFieldSubmitted: (term) {
                    _signupClickValidation();
                  },
                  decoration: InputDecoration(
                    hintText: AppLocalizations.of(context)
                        .translate('hint_confirm_password'),
                  ),
                ),

                //Todo for manage space bewtween control
                new Container(
                  margin: new EdgeInsets.fromLTRB(
                      0.0,
                      MyConstants.vertical_control_space,
                      0.0,
                      MyConstants.vertical_control_space),
                ),

                RaisedButton(
                  onPressed: () {
                    _signupClickValidation();
                  },
                  child: Text(
                      AppLocalizations.of(context).translate("label_signup")),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///Handle signup click flow for validate mendatory field
  void _signupClickValidation() {
    setState(() {
      /*if (_image==null) {
        MyUtils().toastdisplay("Please select Profile Pic");
        return;
      }*/

      if (textFirstnameController.text.isEmpty) {
        MyUtils().toastdisplay(
            AppLocalizations.of(context).translate('msg_enter_first_name'));
        return;
      }

      if (textLastnameController.text.isEmpty) {
        MyUtils().toastdisplay(
            AppLocalizations.of(context).translate('msg_enter_last_name'));
        return;
      }

      if (!MyUtils().validateEmail(context, textEmailController.text)) {
        return;
      }

      if (!MyUtils().validateMobile(context, textPhoneController.text)) {
        return;
      }

      if (!MyUtils().validatePassword(context, textPasswordController.text)) {
        return;
      }

      if (!MyUtils()
          .validatePassword(context, textConfirmPasswordController.text)) {
        return;
      }

      //Password & Confirm password same
      if (textPasswordController.text != textConfirmPasswordController.text) {
        MyUtils().toastdisplay(AppLocalizations.of(context)
            .translate('msg_same_password_confirm'));
        return;
      }

      // Check for internet
      MyUtils().check().then((intenet) {
        if (intenet != null && intenet) {
          _signupPresenter.doSignup(
              context,
              textFirstnameController.text,
              textLastnameController.text,
              textEmailController.text,
              textPhoneController.text,
              selectedCountryCode,
              textPasswordController.text,
              textConfirmPasswordController.text,
              _image);
        } else {
          MyUtils().toastdisplay(AppLocalizations.of(context)
              .translate('msg_no_internet'));
        }
      });
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    textFirstnameController.dispose();
    textEmailController.dispose();
    textPhoneController.dispose();
    textPasswordController.dispose();
    textConfirmPasswordController.dispose();
    super.dispose();
  }

  ///Handle country change evebt from drop down
  void _onCountryChange(CountryCode countryCode) {
    //Todo : manipulate the selected country code here
    selectedCountryCode = countryCode.toString();
    print("New Country selected: " + countryCode.toString());
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  //Dialog for attach image from gallery or camera
  Future<void> _optionsDialogBox() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[
                  GestureDetector(
                    child: new Text('Take a picture'),
                    onTap: openCamera,
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  GestureDetector(
                    child: new Text('Select from gallery'),
                    onTap: openGallery,
                  ),
                ],
              ),
            ),
          );
        });
  }

  ///Open Camera
  openCamera() async {
    Navigator.of(context).pop();
    var picture = await ImagePicker.pickImage(
      source: ImageSource.camera,
    );

    setState(() {
      _image = picture;
    });
  }

  //Open Gallery
  openGallery() async {
    Navigator.of(context).pop();
    var gallery = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );

    setState(() {
      _image = gallery;
    });
  }

  //Handle Api error flow
  @override
  void onSignupError(String errorTxt) {
    MyUtils().toastdisplay(errorTxt);
  }

  //Handle Api success flow
  @override
  void onSignupSuccess(SignupPojo user) {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => OtpPage(getMobileNo: textPhoneController.text,selectedCountryCode: selectedCountryCode,),
        ));
  }
}
