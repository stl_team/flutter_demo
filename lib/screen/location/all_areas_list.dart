import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/models/all_states_list_pojo.dart';
import 'package:rental_app/presenter/all_states_list_presenter.dart';
import 'package:rental_app/utils/my_constants.dart';
import 'package:rental_app/utils/my_utils.dart';
import 'package:rental_app/utils/share_preference.dart';

class GetAllAreas extends StatefulWidget {
  final String title;
  final String city_id;

  GetAllAreas({Key key, this.title, this.city_id}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _GetAllAreasStateFul();
  }
}

class _GetAllAreasStateFul extends State<GetAllAreas>
    implements GetLocationsContract {
  //Declare presenter for get location details
  GetLocationsPresenter _getLocationsPresenter;
  LocationsCommonPojo _locationsPojo;

  @override
  void initState() {
    _apiCallForGetLocations();
    super.initState();
  }

  void _apiCallForGetLocations() {
    // Check for internet
    MyUtils().check().then((intenet) {
      if (intenet != null && intenet) {
        _getLocationsPresenter = new GetLocationsPresenter(this);
        _getLocationsPresenter.doGetAreas(context, widget.city_id);
      } else {
        MyUtils().toastdisplay(
            AppLocalizations.of(context).translate('msg_no_internet'));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: AutoSizeText(
          AppLocalizations.of(context)
              .translate("label_location")
              .toUpperCase(),
          maxLines: 1,
        ),
      ),
      body: _locationsPojo == null
          ? new Center(
              child: new CircularProgressIndicator(),
            )
          : loadGetLocationsData(),
    );
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    super.dispose();
  }

  ///Load and display Areas name data in list
  ListView loadGetLocationsData() {
    return new ListView.builder(
      itemCount: _locationsPojo.data.length,
      itemBuilder: (context, position) {
        return Card(
          child: InkWell(
            onTap: () {
              if ((_locationsPojo.data[position].areasCount != null &&
                  _locationsPojo.data[position].areasCount > 0)) {
                /*Navigator.push(
                    context, MaterialPageRoute(builder: (context) => GetAllAreas(
                  allCitiesPojo: _LocationsPojo.data[position].cities,
                )));*/
              } else {
                MySharePreference().saveStringInPref(
                    MyConstants.PREF_SELECTED_LOCATION,
                    _locationsPojo.data[position].areaName);
                Navigator.pop(context);
              }
            },
            child: Padding(
              padding: const EdgeInsets.all(MyConstants.layout_margin),
              child: AutoSizeText(
                _locationsPojo.data[position].areaName,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: MyConstants.textStyle_common_text,
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  void onGetLocationsError(String errorTxt) {
    MyUtils().toastdisplay(errorTxt);
  }

  @override
  void onGetLocationsSuccess(LocationsCommonPojo pojoData) {
    if (pojoData != null && pojoData.data.length > 0 && this.mounted) {
      setState(() {
        _locationsPojo = pojoData;
      });
    } else {
      MyUtils().toastdisplay(
          AppLocalizations.of(context).translate("msg_no_data_found"));
      Navigator.pop(context);
    }
  }
}
