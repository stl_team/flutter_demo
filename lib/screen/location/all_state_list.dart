import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/models/all_states_list_pojo.dart';
import 'package:rental_app/presenter/all_states_list_presenter.dart';
import 'package:rental_app/utils/my_constants.dart';
import 'package:rental_app/utils/my_utils.dart';
import 'package:rental_app/utils/share_preference.dart';

import 'all_cities_list.dart';

class GetAllStates extends StatefulWidget {
  final String title;

  GetAllStates({Key key, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _GetAllStatesStateFul();
  }
}

class _GetAllStatesStateFul extends State<GetAllStates>
    implements GetLocationsContract {
  //Declare presenter for api call
  GetLocationsPresenter _getLocationsPresenter;
  LocationsCommonPojo _locationsPojo;

  @override
  void initState() {
    _apiCallForGetLocations();
    super.initState();
  }

  void _apiCallForGetLocations() {
    // Check for internet
    MyUtils().check().then((intenet) {
      if (intenet != null && intenet) {
        _getLocationsPresenter = new GetLocationsPresenter(this);
        _getLocationsPresenter.doGetAllStates(context);
      } else {
        MyUtils().toastdisplay(
            AppLocalizations.of(context).translate('msg_no_internet'));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: AutoSizeText(
          AppLocalizations.of(context)
              .translate("label_location")
              .toUpperCase(),
          maxLines: 1,
        ),
      ),
      body: _locationsPojo == null
          ? new Center(
              child: new CircularProgressIndicator(),
            )
          : loadGetLocationsData(),
    );
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    super.dispose();
  }

  ///Load and display States name data in list
  ListView loadGetLocationsData() {
    return new ListView.builder(
      itemCount: _locationsPojo.data.length,
      itemBuilder: (context, position) {
        return Card(
          child: InkWell(
            onTap: () {
              if ((_locationsPojo.data[position].citiesCount != null &&
                  _locationsPojo.data[position].citiesCount > 0)) {
                _navigateToNextScreen(context, position);
              } else {
                MySharePreference().saveStringInPref(
                    MyConstants.PREF_SELECTED_LOCATION,
                    _locationsPojo.data[position].name);

                Navigator.pop(context, [_locationsPojo.data[position].name]);
              }
            },
            child: Padding(
              padding: const EdgeInsets.all(MyConstants.layout_margin),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: AutoSizeText(
                      _locationsPojo.data[position].name,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: MyConstants.textStyle_common_text,
                    ),
                  ),
                  (_locationsPojo.data[position].citiesCount != null &&
                          _locationsPojo.data[position].citiesCount > 0)
                      ? Icon(
                          Icons.navigate_next,
                        )
                      : Container(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  ///Redirect to next screena and handle callback
  _navigateToNextScreen(BuildContext context, tapIndex) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.

    var result = null;

    result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => GetAllCities(
                  state_id: _locationsPojo.data[tapIndex].id.toString(),
                )));

    if (result != null) {
      Navigator.pop(context, [_locationsPojo.data[tapIndex].name]);
    }
  }

  ///Handle api Error flow
  @override
  void onGetLocationsError(String errorTxt) {
    MyUtils().toastdisplay(errorTxt);
  }

  ///Handle api success flow
  @override
  void onGetLocationsSuccess(LocationsCommonPojo pojoData) {
    if (pojoData != null && pojoData.data.length > 0 && this.mounted) {
      setState(() {
        _locationsPojo = pojoData;
      });
    } else {
      MyUtils().toastdisplay(
          AppLocalizations.of(context).translate("msg_no_data_found"));
      Navigator.pop(context);
    }
  }
}
