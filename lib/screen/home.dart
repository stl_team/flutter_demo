import 'package:flutter/material.dart';
import 'package:rental_app/fragments/explore_fragment.dart';
import 'package:rental_app/fragments/my_account_fragment.dart';
import 'package:rental_app/fragments/myads_fragment.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/screen/category/select_main_category.dart';
import 'package:rental_app/screen/signup.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  State<StatefulWidget> createState() {
    return new _MyHomePageStateFul();
  }
}

class _MyHomePageStateFul extends State<MyHomePage> {
  int currentTabIndex = 0;

  //All bottom Tab declare here
  List<Widget> tabs = [ExploreFragment(),SelectMainCategory(),MyAdsFragment(), MyAccountFragment()];

  ///Handle Tab change event here
  onTapped(int index) {
    setState(() {
      if(index==1){
        Navigator.push(
            context,
            MaterialPageRoute(
            builder: (context) => SelectMainCategory(
            title: AppLocalizations.of(context)
                .translate("label_select_category"))));
      }else{
        currentTabIndex = index;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: tabs[currentTabIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: onTapped,
        currentIndex: currentTabIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.explore),
            title: Text(AppLocalizations.of(context).translate("label_explore")),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.camera_alt),
            title: Text(AppLocalizations.of(context).translate("label_sell")),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_to_queue),
            title: Text(AppLocalizations.of(context).translate("label_myads")),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text(AppLocalizations.of(context).translate("label_my_account")),
          )
        ],
      ),
    );
  }
}
