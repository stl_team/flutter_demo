import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/models/main_category_pojo.dart';
import 'package:rental_app/presenter/getcategory_presenter.dart';
import 'package:rental_app/screen/category/select_sub_category.dart';
import 'package:rental_app/utils/my_utils.dart';

class SelectMainCategory extends StatefulWidget {
  final String title;

  const SelectMainCategory({Key key, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _SelectMainCategoryStateFul();
  }
}

class _SelectMainCategoryStateFul extends State<SelectMainCategory>
    implements GetCategoryContract {
  //Api call presenter declare
  GetCategoryPresenter _categoryPresenter;
  GETCategoryPojo getCategoryPojo;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(
            AppLocalizations.of(context).translate("label_select_category")),
      ),
      body: getCategoryPojo == null
          ? new Center(
              child: new CircularProgressIndicator(),
            )
          : loadCategoryGridData(getCategoryPojo),
    );
  }

  @override
  void initState() {
    // Check for internet
    MyUtils().check().then((intenet) {
      if (intenet != null && intenet) {
        _categoryPresenter = new GetCategoryPresenter(this);
        _categoryPresenter.doGetCategory(context);
      } else {
        MyUtils().toastdisplay(
            AppLocalizations.of(context).translate('msg_no_internet'));
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    super.dispose();
  }

  ///Load category data and set into the Grid
  GridView loadCategoryGridData(GETCategoryPojo categoryData) {
    return new GridView.builder(
        itemCount: categoryData.data.length,
        gridDelegate:
            new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          return new GestureDetector(
            child: new Card(
              elevation: 5.0,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(categoryData.data[index].categoryName,maxLines: 1,textAlign: TextAlign.center,),
                ],
              ),
            ),
            onTap: () {
              if(!categoryData.data[index].subCategories.isEmpty){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SelectSubCategory(
                          title: categoryData.data[index].categoryName,
                          subCategories: categoryData.data[index].subCategories,)));
              }
            },
          );
        });
  }

  ///Handle api error flow
  @override
  void onGetCategoryError(String errorTxt) {
    MyUtils().toastdisplay(errorTxt);
  }

  ///Handle api success flow
  @override
  void onGetCategorySuccess(GETCategoryPojo pojoData) {
    getCategoryPojo = pojoData;
    setState(() {
      print('UI Updated');
    });
  }
}
