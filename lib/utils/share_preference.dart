import 'package:shared_preferences/shared_preferences.dart';

class MySharePreference{

  ///
  /// For to handle Save and Get String,Integer,Boolean value in Preference
  ///


  Future saveStringInPref(String key,String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key,value);
  }

  Future saveBoolInPref(String key,bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(key,value);
  }

  Future saveIntegerInPref(String key,int value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(key,value);
  }


  Future<String> getStringInPref(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(key) ?? "";
  }

  Future<bool> getBoolInPref(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool(key) ?? false;
  }

  Future<int> getIntegerInPref(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getInt(key) ?? 0;
  }

  /// ------------------------------------------------------------
  /// Method that returns the user decision to allow notifications
  /// ------------------------------------------------------------
   clearAllPref() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.clear();
  }

}