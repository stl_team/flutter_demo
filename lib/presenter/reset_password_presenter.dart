import 'package:flutter/cupertino.dart';
import 'package:rental_app/models/reset_password_pojo.dart';
import 'package:rental_app/network/rest_ds.dart';

abstract class ResetPasswordContract {
  void onResetSuccess(ResetPasswordPojo pojoData);
  void onResetError(String errorTxt);
}

//ResetPasswordresenter for call and handle api flow
class ResetPasswordresenter {
  ResetPasswordContract _view;
  RestDatasource api = new RestDatasource();
  ResetPasswordresenter(this._view);

  doResetPassword(BuildContext getContext,String mobileno,String country_code,String password, String password_confirmation) async{
    try {
      await api.resetPassword(getContext,mobileno,country_code,password,password_confirmation).then((ResetPasswordPojo pojoData) {
        _view.onResetSuccess(pojoData);
      });
    } catch(error){
      print('Error occured: $error');
      _view.onResetError(error.toString());
    }
  }
}