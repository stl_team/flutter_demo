import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:rental_app/models/signup_pojo.dart';
import 'package:rental_app/network/rest_ds.dart';

abstract class SignupContract {
  void onSignupSuccess(SignupPojo user);

  void onSignupError(String errorTxt);
}

//SignupPresenter for call and handle api flow
class SignupPresenter {
  SignupContract _view;
  RestDatasource api = new RestDatasource();

  SignupPresenter(this._view);

  doSignup(BuildContext getContext,String first_name, String last_name,String email,
      String mobile_no,String country_code, String password, String password_confirmation, File fileProfile) async {
    try {
      await api.signup(getContext,first_name, last_name,email,
          mobile_no,country_code, password, password_confirmation, fileProfile).then((SignupPojo user) {
        _view.onSignupSuccess(user);
      });
    } catch (error) {
      print('Error occured: $error');
      _view.onSignupError(error.toString());
    }
  }
}
