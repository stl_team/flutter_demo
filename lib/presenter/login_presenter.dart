import 'package:flutter/cupertino.dart';
import 'package:rental_app/models/user_pojo.dart';
import 'package:rental_app/network/rest_ds.dart';

abstract class LoginContract {
  void onLoginSuccess(UserPojo user);
  void onLoginError(String errorTxt);
}

//LoginPresenter for call and handle api flow
class LoginPresenter {
  LoginContract _view;
  RestDatasource api = new RestDatasource();
  LoginPresenter(this._view);

  doLogin(BuildContext getContext,String mobile_no,String country_code, String password) async{
    try {
      await api.login(getContext,mobile_no,country_code, password).then((UserPojo user) {
        _view.onLoginSuccess(user);
      });
    } catch(error){
      print('Error occured: $error');
      _view.onLoginError(error.toString());
    }
  }
}