import 'package:flutter/cupertino.dart';
import 'package:rental_app/models/all_states_list_pojo.dart';
import 'package:rental_app/network/rest_ds.dart';

abstract class GetLocationsContract {
  void onGetLocationsSuccess(LocationsCommonPojo pojoData);

  void onGetLocationsError(String errorTxt);
}

//Location presenter for call State,Cities,Area and handle api flow
class GetLocationsPresenter {
  GetLocationsContract _view;
  RestDatasource api = new RestDatasource();

  GetLocationsPresenter(this._view);

  doGetAllStates(
      BuildContext getContext) async {
    try {
      await api.getAllStates(getContext)
          .then((LocationsCommonPojo pojoData) {
        _view.onGetLocationsSuccess(pojoData);
      });
    } catch (error) {
      print('Error occured: $error');
      _view.onGetLocationsError(error.toString());
    }
  }


  doGetCities(
      BuildContext getContext,state_id) async {
    try {
      await api.getCities(getContext,state_id)
          .then((LocationsCommonPojo pojoData) {
        _view.onGetLocationsSuccess(pojoData);
      });
    } catch (error) {
      print('Error occured: $error');
      _view.onGetLocationsError(error.toString());
    }
  }

  doGetAreas(
      BuildContext getContext,city_id) async {
    try {
      await api.getAreas(getContext,city_id)
          .then((LocationsCommonPojo pojoData) {
        _view.onGetLocationsSuccess(pojoData);
      });
    } catch (error) {
      print('Error occured: $error');
      _view.onGetLocationsError(error.toString());
    }
  }
}
