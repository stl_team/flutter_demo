import 'package:flutter/cupertino.dart';
import 'package:rental_app/models/get_otp_pojo.dart';
import 'package:rental_app/network/rest_ds.dart';

abstract class GetOTPContract {
  void onGetOTPSuccess(GetOtpPojo pojoData);
  void onGetOTPError(String errorTxt);
}

//OTP presenter for call and handle api flow
class GetOTPPresenter {
  GetOTPContract _view;
  RestDatasource api = new RestDatasource();
  GetOTPPresenter(this._view);

  doGetOTP(BuildContext getContext,String mobileno,String country_code) async{
    try {
      await api.getOTP(getContext,mobileno,country_code).then((GetOtpPojo pojoData) {
        _view.onGetOTPSuccess(pojoData);
      });
    } catch(error){
      print('Error occured: $error');
      _view.onGetOTPError(error.toString());
    }
  }
}